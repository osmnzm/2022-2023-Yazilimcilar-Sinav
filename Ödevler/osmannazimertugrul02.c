#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <conio.h>


void dosyaOlustur(){    
    FILE *dosya = fopen("defter.txt", "w");
    printf("Dosya olusturuldu...\n");
    fclose(dosya);
}

void yaziEkle(){
    char inputString[1000];
    FILE *dosya = fopen("deneme.txt", "a");

    printf("Yazinizi giriniz:");
    scanf( "%s", &inputString);
    fprintf(dosya, "%s", inputString);
    fclose(dosya);

}

void notlariGoster(){
        FILE *dosya = fopen("deneme.txt", "r");
        int ch = getc(dosya);

        while (ch != EOF){
            putchar (ch);
            ch = getc(dosya);
        }

        if (feof(dosya)){
            printf("\nOkuma basarili...");
        }

        fclose(dosya);
        getchar();
}

void toDoEkle(){
    FILE *dosya1 = fopen("toDoDosya.txt", "w");
    char aktivite[20], devam;
    int no,sira;

    if (dosya1 == NULL){
        printf("Sunucu bulunamadı..");
    }
    else {
        while(1){
            printf("Kacinici aktivite : ");
            scanf("%d", &sira);
            printf("Aktivite adi : ");
            scanf("%s", &aktivite);
            fprintf(dosya1,"%d\t%5s\n", sira, aktivite );
            printf("Deavam etmek istiyor musunuz?");
            devam = getche();
            if (devam == 'h') break;
            printf("\n");
        }
        fclose(dosya1);
        printf("\n");
    }
}

void toDoGoster(){
    FILE *dosya1 = fopen("toDoDosya.txt", "r");
    char aktivite[20];
    int sira;

    if (dosya1 == NULL){
        printf("Sunucu bulunamadı");
    }
    else{
        printf("Sira\tAktivite\n");
        printf("-----------------\n");
        while(fscanf(dosya1,"%d%s",&sira,aktivite ) != EOF){
        printf("%d\t%s\n",sira, aktivite);}

        fclose(dosya1);
    }
}
void toDoKaldir(){
    int kaldir = remove("toDoDosya.txt");
    if (kaldir == 0){
        printf("Tebrikler, listenizdeki her seyi yaptiniz!");
    }
    else{
        printf("Daha yurunecek cok yol var...");
    }
}



int main(){
    int girisIslem, defterSecim, toDoSecim;
    char inputString[1000];
    printf("Hos geldiniz...\n[1]Not Defteri\n[2]ToDo List\nLutfen islem seciniz:");
    scanf("%d", &girisIslem);

    if (girisIslem == 1)
    {
        
        printf("Not defterin hos geldiniz!\n[1]Dosya Olustur\n[2]Yazi Ekle\n[3]Notlari Goster\nLutfen islem seciniz:");
        scanf("%d", &defterSecim);

        switch (defterSecim){
            case 1:

            dosyaOlustur();
            break;

            case 2:

            yaziEkle();
            break;

            case 3:

            notlariGoster();
            break;


        }
    }

    else if (girisIslem == 2)
    {
      printf("To Do listenize hos geldiniz!\n[1]Liste olustur\n[2]Listeyi Goruntule\n[3]Yapilmis olarak isaretle\nLutfen islem seciniz:");
      scanf("%d", &toDoSecim);

      switch(toDoSecim){
        case 1:

        toDoEkle();
        break;

        case 2:

        toDoGoster();
        break;

        case 3:
        toDoKaldir();
        break;
      }

    }
    else {
        printf("Lutfen gecerli bir islem seciniz..");
    }

    return 0;
}